import React,{useEffect, useState} from 'react';
import Error from './Error';
import styled from '@emotion/styled';
import useMoneda from '../hooks/useModenas';
import useCriptomoneda from '../hooks/useCriptomoneda';
import Axios from 'axios';

const Boton = styled.input`
    margin-top:20px;
    font-weight:bold;
    font-size:20px;
    padding:10px;
    background-color: #66a2fe;
    border:none;
    width: 100%;
    border-radius:10px;
    color:#fff;
    transition: background-color .3s ease;

        &:hover{
          background-color:#2E5373;
         cursor:pointer;
        }
    `;
const Formulario = ({guardarMoneda, guardarCriptomoneda}) => {
    //state del listado de criptomonedas

    const [listacripto,guardarCriptomondas]=useState([]);

    //state para la validacion

    const [error,guardarError]=useState(false);

    const MONEDAS=[
        {codigo: 'USD', nombre:'Dolar de Estados Unidos'},
        {codigo: 'MXN', nombre:'Peso Mexicano'},
        {codigo: 'ARS', nombre:'Peso Argentino'},
        {codigo: 'EUR', nombre:'Euro'},
        {codigo: 'GBP', nombre:'Libra Esterlina'},
    ]
//extraer useMoneda (traemos tal cual el orden, le podemos cambiar los nombres a las variables, pero las usaremos tal cual el orden)

const [ moneda, SelectMonedas] = useMoneda('Eligue tu moneda', '', MONEDAS);
//utilizar criptomoneda
const [criptomoneda, SelectCripto]= useCriptomoneda('Elige tu Criptomoneda', '',listacripto);

useEffect(() => {
    const consultarAPI = async ()=>{
       const url= `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;
       const resultado= await Axios.get(url);
       guardarCriptomondas(resultado.data.Data);
    }
    consultarAPI();
    
}, []);

//cuando el usuario hace submit
const cotizarMoneda =e=>{
    e.preventDefault();
    //revisar que esten llenos los valores

    if ( criptomoneda.trim()==='' || criptomoneda.trim()===''){
        guardarError(true);
        return;
    }
    //En dado caso que pase la validación
    guardarError(false);

    //pasar los datos al componente principal
    guardarMoneda(moneda);
    guardarCriptomoneda(criptomoneda);
}

    return ( 
        <form
            onSubmit={cotizarMoneda}
        >
            {error ? <Error mensaje='Todos los campos son obligatorios'/> :null}
            <SelectMonedas/>
            <SelectCripto/>
            <Boton
            type="submit"
            value="Calcular"
            />
        </form>
     );
}
 
export default Formulario;