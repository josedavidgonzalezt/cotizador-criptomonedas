import React from 'react';
import styled from '@emotion/styled';

const ResultadoDiv=styled.div`
    color:#fff;
    font-family:Arial, Helvetica, sans-serif;
    background-color:rgba(46,83,115,.5);
    box-shadow: 1px 2px 15px 6px black;
    margin-top: 1rem;
    padding:2rem;
`;
const Info=styled.p`
    font-size:18px;

    span{
        font-weight:bold;
    }
`;

const Precio=styled.p`
    font-size:50px;
        span{
            font-weight:bold;
        }
`;

const Cotizacion = ({resultado}) => {
    if(Object.keys(resultado).length===0)return null;
    return ( 
        <ResultadoDiv>
            <Precio>El precio es: <span>{resultado.PRICE}</span></Precio>
            <Info>Precio más alto del día: <span>{resultado.HIGHDAY}</span></Info>
            <Info>Precio más bajo del día: <span>{resultado.LOWDAY}</span></Info>
            <p>Variación últimas 24 horas: <span>{resultado.CHANGEPCT24HOUR}</span></p>
            <Info>Última Acualización: <span>{resultado.LASTUPDATE}</span></Info>
        </ResultadoDiv>
    );
}
 
export default Cotizacion;